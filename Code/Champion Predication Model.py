#!/usr/bin/env python
# coding: utf-8

# # Champion Predication Model
# #By- Aarush Kumar
# #Dated: September 18,2021

# In[5]:


from IPython.display import Image
Image(url='https://i.pinimg.com/originals/96/f2/2d/96f22d3c7da680f8369d345f548f8837.jpg')


# In[6]:


import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')


# In[7]:


df = pd.read_csv('/home/aarush100616/Downloads/Projects/Predicting Champion Model/ucl_stats.csv')


# In[8]:


df


# In[9]:


df.head()


# In[10]:


df.shape


# In[11]:


df.size


# In[12]:


df.info()


# In[13]:


df.isnull().sum()


# In[14]:


df.describe()


# In[15]:


df.describe().T


# In[16]:


# number of sample in each class
df.champions.value_counts().plot(kind = 'bar');


# In[17]:


df['win_match_ratio'] = (df['wins'] + 1)/ df['match_played']
df['gs_match_ratio'] = (df['goals_scored'] + 1)/ df['match_played']
df['gc_match_ratio'] = (df['goals_conceded'] + 1)/ df['match_played']
df['win_gs_ratio'] = (df['wins'] + 1)/(df['goals_scored'] + 1)
df['win_lost_ratio'] = (df['wins'] + 1)/(df['losts'] + 1)
df['gs_gc'] = (df['goals_scored'] - df['goals_conceded']) + 0.1
df['wins_draws_ratio'] = (df['wins'] +  1) / (df['draws'] + 1)
df['gs_gd'] = (df['goals_scored'] + 1) + (df['gd'])


# In[18]:


df.drop(['year', 'team'], axis = 1, inplace = True)


# In[19]:


x = df.drop('champions', axis = 1)
y = df['champions']


# ## Feature Selection

# In[20]:


from sklearn.feature_selection import mutual_info_regression

def make_mi_scores(X, y):
    mi_scores = mutual_info_regression(X, y)
    mi_scores = pd.Series(mi_scores, name="MI Scores", index=X.columns)
    mi_scores = mi_scores.sort_values(ascending=False)
    return mi_scores

def plot_mi_scores(scores):
    scores = scores.sort_values(ascending=True)
    width = np.arange(len(scores))
    ticks = list(scores.index)
    plt.barh(width, scores)
    plt.yticks(width, ticks)
    plt.title("Mutual Information Scores")


# In[21]:


# mutual info regression scores
mi_score = make_mi_scores(x, y)
print(mi_score)


# In[22]:


plot_mi_scores(mi_score)


# ## Balancing Imbalance Distribution Problem

# In[23]:


from imblearn.over_sampling import SMOTE
smote = SMOTE()
x_sm, y_sm= smote.fit_resample(x, y)


# In[24]:


pd.DataFrame(y_sm).champions.value_counts().plot(kind = 'bar');


# In[25]:


from sklearn.model_selection import train_test_split
x_train,x_test,y_train,y_test = train_test_split(x_sm, y_sm,random_state = 31)


# In[26]:


from sklearn.metrics import classification_report, plot_confusion_matrix, plot_roc_curve
def report(classifier, x_test = x_test, y_test = y_test):
    print("Classification Report: \n")
    y_preds = classifier.predict(x_test)
    print(classification_report(y_test, y_preds))
    print("Confusion Matrix: \n")
    plot_confusion_matrix(classifier, x_test, y_test, cmap=plt.cm.Blues)
    print("ROC: \n")
    plot_roc_curve(classifier, x_test, y_test) 
    plt.plot([0, 1], [0, 1], color='red', linestyle='--')


# ## Logistic Regression

# In[27]:


from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression
log_clf = LogisticRegression(max_iter = 100,
                             C = 5.428675439323859,
                             penalty='l1',
                             solver='liblinear',
                             random_state = 41).fit(x_train, y_train)

cv_lr = cross_val_score(log_clf, x_train, y_train, cv = 10)
lr_score = np.mean(cv_lr)
print("LR Score: ", lr_score)


# In[28]:


report(log_clf)


# ## KNN

# In[29]:


from sklearn.neighbors import KNeighborsClassifier

# parameter tuned by gridsearchcv
knn_clf = KNeighborsClassifier(algorithm='auto',
                                leaf_size=10,
                                n_neighbors=2,
                                p = 1).fit(x_train, y_train)

cv_knn = cross_val_score(knn_clf, x_train, y_train, cv = 10)
knn_score = np.mean(cv_knn)
print("KNN Score: ", knn_score)


# In[30]:


report(knn_clf)


# ## SVC

# In[31]:


from sklearn import svm

# parameter tuned by gridsearchcv
svc_clf = svm.SVC(C = 233.57214690901213,
                  degree = 2,
                  kernel = 'rbf',
                  random_state = 7).fit(x_train, y_train)

cv_svc = cross_val_score(knn_clf, x_train, y_train, cv = 10)
svc_score = np.mean(cv_svc)
print("SVC Score: ", svc_score)


# In[32]:


report(svc_clf)


# ## Random Forest

# In[33]:


from sklearn.ensemble import RandomForestClassifier

# parameters were taken by randomizedsearchcv
rand_clf = RandomForestClassifier(n_estimators=1000,
                                 min_samples_split = 4,
                                 min_samples_leaf = 1,
                                 max_depth = None,
                                 random_state = 35).fit(x_train, y_train)

cv_rf = cross_val_score(rand_clf, x_train, y_train, cv = 10)
rf_score = np.mean(cv_rf)
print("RF Score: ", rf_score)


# In[34]:


report(rand_clf)


# ## Gradient Boost

# In[35]:


from sklearn.ensemble import GradientBoostingClassifier

gbc_clf = GradientBoostingClassifier(learning_rate=0.1,
                                 loss='deviance',
                                 max_depth=2,
                                 min_samples_leaf=5,
                                 min_samples_split=2,
                                 n_estimators=500,
                                 random_state=31).fit(x_train, y_train)

cv_gbc = cross_val_score(gbc_clf, x_train, y_train, cv = 10)
gbc_score = np.mean(cv_gbc)
print("GBC Score: ", gbc_score)


# In[36]:


report(gbc_clf)


# ## LightBGM

# In[37]:


from lightgbm import LGBMClassifier
lgbm = LGBMClassifier().fit(x_train, y_train)
cv_lgbm = cross_val_score(lgbm, x_train, y_train, cv = 10)
lgbm_score = np.mean(cv_lgbm)
print("Lgbm Score: ", lgbm_score)


# In[38]:


report(lgbm)


# ## Score Analysis

# In[39]:


score_dict= {'Logistic Regression':lr_score,
             'KNN':knn_score,
             'SVC':svc_score,
             'Random Forest': rf_score,
             'Gradient Boost':gbc_score,
             'LightBGM':lgbm_score}
pd.DataFrame.from_dict(score_dict, orient = 'index', columns = ['Score'])

